﻿
var RedisStore;



function setSessionStore(app, express) {

  RedisStore = require("connect-redis")(express);
  console.log("set up session on Grantina Data\n\n");
  //localhost
  if (!process.env.REDIS_URL) {
    onLocalHost(app, express);
    return;
  }

  app.use(express.session({
    store: new RedisStore({
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      pass: process.env.REDIS_PASS
    }),
    secret: process.env.REDIS_SECRET
  }));
};




function onLocalHost(app, express) {
  console.log("using memory session");
  app.use(express.session({secret: "aviguy"}));
}

exports.init = setSessionStore;