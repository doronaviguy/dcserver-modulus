﻿
var red, blue, reset;
red = '\033[31m';
blue = '\033[34m';
reset = '\033[0m';


var AWS = require('aws-sdk')
    fs = require('fs'),
    request = require('request'),
    url = require('url');

var amazonConfig = {
  accessKeyId: process.env.AMAZON_ID || 'AKIAJEQZEVEHLNTOOC4A',
  secretAccessKey: process.env.AMAZON_SECRET || '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',
  baseStorageUrl: process.env.AMAZON_S3_URL  || "https://s3.amazonaws.com/dcdrawing/"
}
var CDNurl = process.env.AMAZON_PUBLIC_URL;

console.log('CDNurl =' + CDNurl);


console.log("amazon vars");
console.log("process.env.AMAZON_PUBLIC_URL =>" + process.env.AMAZON_PUBLIC_URL);


AWS.config.update(amazonConfig);
AWS.config.update({ region: 'us-east-1' });

var s3 = new AWS.S3();


var bucketName = "dcdrawing";
///////////////////////////////////////////////////////////////////////////////////
/*My Promise Light*/
function Promise() {
  var clbk;
  var onFail;
  return {
    then: function (func) { clbk = func; },
    fail: function (func) { onFail = func; },
    failure: function (err) { onFail(err); },
    resolve: function (prm) { clbk(prm); }
  }
}
///////////////////////////////////////////////////////////////////////////////////
// key => path on CDN
// data => data
function upload(key, value) {
  //console.log("upload() enter \n[key] = "+ key);
  
  promise =  Promise();


  var data = { Bucket: bucketName, Key: key, Body: value, ACL: 'public-read' };
  s3.client.putObject(data, function (err, data) {
    //console.log("putObject() inside");
    //console.dir(err);

    if (err) {
      console.log("Error uploading data: ", err);
      promise.resolve("ok");
    }
    else {
      console.log("upload sucessful at " + amazonConfig.baseStorageUrl + key);
      
      promise.resolve("ok");
    }
  });
  //console.log("upload() leave");
  return promise;
}

///////////////////////////////////////////////////////////////////////////////////

function pushDrawing(path, data) {
  //console.log("pushDrawing()");
  var promise =  Promise();
  var it = path;
  var subs = it.split("/");
  var commentId = subs[subs.length - 1];
  var postId = subs[subs.length - 2];
  console.log(' == > drawings/' + postId + '/' + commentId);

  var key = postId + '/' + commentId + '.js';
  upload(key, data).then(function (res) {

    //console.log("pushDrawing()  after Promise Then [postId, commentId]" + postId+ ":" + commentId);
    updateCommentList(postId, commentId).then(function() {
      promise.resolve();
    });

  });;
  return promise;
}
///commentId => List file to mapp all the post ids for the comment
///valToAdd =>  New post Id
///////////////////////////////////////////////////////////////////////////////////
function updateCommentList(postId, valToAdd) {
  var promise = Promise();
  //console.log("updateCommentList()");
  request.get(amazonConfig.baseStorageUrl+ postId + ".js",
    function (err, rslt) {
      if (err) {
        console.log(err);
        return;
      }
        
      var body = rslt.body;
      //console.log(" updateCommentList() body => " + body);

      if (body.indexOf("Error") > -1) {
        upload(postId + '.js', valToAdd).then(function () {
          console.log("updateCommentList() Ok [new file]");
          printUrl(amazonConfig.baseStorageUrl + postId + ".js");
          promise.resolve();
          return;
        });
        promise.resolve();
        return;
      }
      /* this case  comment Id already exits*/
      if (body.indexOf(postId) > -1) {
        promise.resolve();
        return;
      }
      body += "," + valToAdd;

      upload(postId + '.js', body).then( function() {
        console.log("updateCommentList() [OK] update success");
        printUrl(amazonConfig.baseStorageUrl + postId + ".js");
        promise.resolve();
        return;
      });

    });
  return promise;
}

function printUrl(url) {
  setTimeout(function () {
    request.get(url, function (err, rslt) {
      if (err) 
        throw err;
      //console.log("rslt.body = " + rslt.body);
    });
  }, 3000);

}

function getDrawingList(postId) {
  var promise = Promise();
  
//  console.log("getDrawingList( " + postId + " ) = " + amazonConfig.baseStorageUrl + postId + ".js");
  request.get(amazonConfig.baseStorageUrl + postId + ".js",
    function (err, res) {
      if (err) {
        promise.resolve(null);
        return;
      }
      if (!res.body) {
        console.log(" res.body is NULL");
        return promise.resolve(null);
      }
      if (res.body.indexOf("<") > -1) {
        //console.log(" res.body is XML ,  not exist");
        return promise.resolve(null);
      }

      //console.log(" res.body = " + res.body);
      var urls = res.body.split(',');
      urls = urls.map(function (it) {
        return CDNurl + postId + '/' + (it + '.js');
      });

      //console.dir(urls);
      promise.resolve(urls);
    });
  return promise;
}

function getBaseurl() {
  return amazonConfig.baseStorageUrl;
}
/// commentId
//  returns URL
function getPostsByCommentId(commentId) {
  return getBaseurl() + commentId + '.js';
}

///////////////////////////////////////////////////////////////////////////////////
/*Exporsts*/
exports.uploadDrawing = pushDrawing;
exports.getBaseurl = getBaseurl;
exports.getPostsByCommentId = getPostsByCommentId;
exports.getDrawingList = getDrawingList;