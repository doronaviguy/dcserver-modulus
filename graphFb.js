/// <reference path="./nodelib/node.js"/>

console.log("DC Server Entery Point");





///////////////////////////////////////////////////////////////////////////////////////////
var express = require('express')
  , app = module.exports = express()
  , websiteRotuer = require('./website')
  , redisSessionStore = require('./session-store-redis-af')
  , extensionApi = require('./extensions-api.js');


app.use(express.limit('5mb'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
redisSessionStore.init(app, express);

app.use(app.router);
//app.use(express.logger('dev'));

app.use(express.static(__dirname + '/public'));
app.use(express.favicon());

app.configure('development', function () {
  console.log("devolpement");
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function () {
  console.log("production !###########################################");
  app.use(express.bodyParser({ uploadDir: express.static(process.env.TEMP_DIR), keepExtensions: true }));
  app.use("/upload", express.static(process.env.TEMP_DIR));
  app.use(express.errorHandler());
});

app.all('/', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});


var auth = express.basicAuth(function (user, pass) {
  return (user == "doron" && pass == "cash77");
}, 'Welcome to dcapi');

// Routes

websiteRotuer.init(app, auth);
extensionApi.init(app);



//////////////////////////////////////////////////////////////////////////////////
process.on('uncaughtException', function (err) {
  console.log('Caught exception: ' + err);
});

var port = process.env.PORT || 80;

app.listen(port);

console.log("Wellcome to Dc Server listening on Port:%d, IP:%d", port, process.env.IP);


