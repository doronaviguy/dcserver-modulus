

var usersDb = (function () {

  var nano = require('nano')('https://dcdb.iriscouch.com');
  var users = nano.use('users');
  // and insert a document in it

  function insertNewUser(user) {

    return;
    console.log(user.id);
    var now = new Date();


    users.get(user.id, { revs_info: true }, function (err, body) {

      if (err) {
        console.log(err);
        return;
      }

      console.log('==>users.get body= ' + console.dir(body));

      if (err) { /*not exists insert a new one*/
        users.insert({ id: user.id, name: user.name, created: now.getDate(), activity: 0 }, user.id + "",
        function (err, body, header) {
          if (err) {
            console.log('[users.insert] ', err.message);
            return;
          }
          console.log('you have inserted the a new User to Users table.')

        });
      } else { 
        
      }
    });
  }

  function getUsers(clbk) {
    var dataRows = [];
    users.list(function (err, body) {
      if (!err) {
        body.rows.forEach(function (doc) {
          dataRows.push(doc);
        });
        clbk(dataRows);
      } else
        clbk([], "No rows in the database");
    });
  }

  return {
    insertUser: insertNewUser,
    getUsers: getUsers
  };

})();


exports.insertUser = usersDb.insertUser;
exports.getUsers = usersDb.getUsers;



