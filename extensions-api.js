﻿
var graph = require('./fbgraph-doron')
  , usersDb = require('./usersDb')
  , q = require("promised-io")
  , conf = require('./config').getConfig()
  , fs = require('fs')
  , path = require('path')
  , amazonClient = require('./amazon-sdk-client')
  , mongo = require('./mongo');





/*
 Methods to be called from the extnsion all XHR.....
*/

exports.init = function (app) {



  app.get('/auth/facebook', function (req, res) {

    //console.log('==> /auth/facebook  trying to login req.query.code=' + req.query.code);

    //console.log(req.session);
    if (req.params.clean)
      req.session = null;


    if (req.session['auth']) {
      var d = new Date();
      console.log(d + '=> user is auth');
      if (!req.xhr) {
        res.send('<script>window.close()</script>');
        return;
      }
      res.send("ok");
      return;
    }

    //console.log("conf.redirect_uri = " + conf.redirect_uri);
    if (!req.query.code) {

      var authUrl = graph.getOauthUrl({
        "client_id": conf.client_id,
        "redirect_uri": conf.redirect_uri,
        "scope": "publish_stream"
      });
      

      if (!req.query.error) { //checks whether a user denied the app facebook login/permissions

        //console.log("redirect to =>" + authUrl);
        res.redirect(authUrl);
      } else {
        console.log("req.query.error = " + req.query.error);
      }
      return;
    }
  });
  ///////////////////////////////////////////////////////////////////////////////////
  app.get('/auth/facebookCallback', function (req, res) {

    //console.log("/auth/facebookCallback");
    // code is set
    // we'll send that and get the access token

    if (req.query.error) {
      res.send(req.query.error);
      return;
    }
    graph.authorize({
      "client_id": conf.client_id
    , "redirect_uri": conf.redirect_uri
    , "client_secret": conf.client_secret
    , "scope": 'publish_stream'
    , "code": req.query.code
    , "session": req.session
    },
    function (err, facebookRes) {
      if (err) {
        console.log(' Authorize Error =>');
        console.log(err);
        return;
      }
      //console.log(' Authorize Response ');
      //console.log(facebookRes);
      req.session['auth'] = true;
      res.redirect('/loggedIn');
    });
  });
  ///////////////////////////////////////////////////////////////////////////////////
  // user gets sent here after being authorized
  app.get('/loggedIn', function (req, res) {

   // console.log(req.params);

    //console.log('==> User is Authroised !!! ');
  

    graph.get("me", null, req.session, function (err, dtls) {
      //console.log("graph returend");
      if (err) {
        console.log("err = ");
        console.log(err);
        res.json(err);
        return;
      }
      console.log("user logged in " + (dtls.name) + " | " + dtls.id);
      req.session.auth = true;
      req.session.fb.name = dtls.name;
      req.session.fb.id = dtls.id;
      req.session.fb.locale = dtls.locale;
      
      mongo.addUser(dtls.name, dtls.id);

      dtls = dtls || { id: 0 };
      if (!req.xhr) {
        //console.log(" not XHR (auth dialog)");
        var wincode = "<script>window.close()</script>";
        res.send(wincode);
        return;
      } 
      res.send("loggedIn id="+ dtls.id);
    });

  });

  app.get('/getinfo', function (req, res) {

    var data = {};
    data.drawingUrl = process.env.AMAZON_S3_URL;
    res.json(data);
  });

  app.get("/my", function (req, res) {

    graph.get("me", null, req.session, function (err, dtls) {
      console.log("graph returend");
      if (err) {
        console.log("err = " + err);
        res.json(err);
        return;
      }

      //console.log('==>graph me response : dtls =' + dtls);
      console.log(dtls);
      if (dtls) {

        req.session.fb = req.session.fb || {};

        req.session.fb.name = dtls.name;
        graph.setFbId(dtls.id, req.session);
        //res.send("logged in ..... as fbid => " + dtls.id);
      }
      if (!req.xhr) {

        var wincode = "<scirpt>alert(1);window.opener.isAppAuth=true;window.close()</script>";
        res.send(wincode);
        return;
      }
      res.send(dtls);
    });

  });

  ///////////////////////////////////////////////////////////////////////////////////

  app.get('/auth/permmsions', function (req, res) {

    console.log("/auth/permmsions  called");
    fs.readFile(__dirname + '/public/login.html', 'utf8', function (err, text) {
      if (err)
        res.send("oops");
      res.send(text);
    });

  });

  /*
    Get Avialable drawings for Post ID
  */
  //////
  app.get('/drawings/:postId', function (req, res) {

    var postId = req.params.postId || "Nan";
    //console.log("/drawings/:postId  = " + postId);


    if (!postId) {
      res.send("invalid postID ");
      return;
    }

    amazonClient.getDrawingList(postId).then(function (urls) {

    //  console.log("amazonClient.getDrawingList returned");

      if (!urls) {
        return res.send("");
      }
   //  console.log("amazonClient.getPostsByCommentId returned urls.length" + urls.length);
      res.send(urls);
    });


  });
  ///////////////////////////////////////////////////////////////////////////////////
  app.get("/resetsession", function (req, res) {

    req.session.destroy();
    res.send("seesion-destroyed");

  });

  app.get('/getss', function (req, res) {

    res.send(req.session || "nada");
  });

  ///////////////////////////////////////////////////////////////////////////////////
  var badOath = false;

  app.post('/drawings', function (req, res) {

    
    console.log('drawings/ [POST]');
    
    if (!graph.isAuth(req.session)) {
      
      console.log("/drawings/ [POST] => (isAuth() faild no token in sessiosn)");
      res.send("noAuth_no_session");
      return;
    }
    

    if (!req.body.postId) {
      console.log("/drawings/ [POST] => (no postId id recived)");
      res.send('no postId id recived') //todo validaiton
      return;
    }
    var commentText = "Wrote somthing on your ....";
    if (req.body.cmntTxt)
      commentText = req.body.cmntTxt;

    var photoPost = {
      message: commentText
    };

    graph.post('/' + req.body.postId + '/comments', photoPost, req.session, function (err, userNewCmntId) {

      if (err) {
        console.log("/drawings/ [POST] => ( graph post error)");
        console.log(err);

        if (err.code == 200) {
          console.log("/drawings/ [POST] => (user have no extended premissions)");
          res.send('no-premssions');
          return;
        }
        if (err.code == 2) {
          console.log("unknow error code 2, contniue as there was no error, postId = " + req.body.postId);
          var msg = { newCmntId: '100' };
          res.json(msg);
          console.log("/drawings/ [POST] => (SEMI-OK)");
          return;
        }
        if (!badOath) {
          req.session.destroy();
          badOath = true;
          console.log("/drawings/ [POST] => (noAuth_10)");
          res.send('noAuth_10');
          return;
        } else {
          req.session.destroy();
          console.log("/drawings/ [POST] => (noAuth_11)");
          res.send('noAuth_11');
          return;
        }
      }

      badOath = false;
      //console.log(userNewCmntId);

      if (!userNewCmntId.id) {
        console.log("/drawings/ [POST] => (bad graph response)");
        res.send('bad graph request');
        return;
      }
      mongo.addAction(req.session.fb.id);

      //console.log('new comment id is =>' + userNewCmntId.id)
      var pos = userNewCmntId.id.indexOf('_');
      if (pos > 0) {
        var newCmntId = userNewCmntId.id.substr(pos + 1, userNewCmntId.id.length - pos);
        console.log('==>newCmntId = ' + newCmntId)
        syncDrawingToCDN(res, req.body.postId, newCmntId, req.body.data);
        var msg = { newCmntId: newCmntId };
        res.json(msg);
        console.log("/drawings/ [POST] => (OK)");
      } else {
        console.log("/drawings/ [POST] => (bad graph response #2)");
        res.send("bad new comment id");
      }
    });
  });
  ///////////////////////////////////////////////////////////////////////////////////
  //TODO auto generate tmp file
  var publishPohotCount = 0;
  app.post('/publishphoto', function (req, res) {

    console.log('/publishphoto');
    

    if (!graph.isAuth(req.session)) {
      console.log('/publishphoto => (No Auth in session)');
      res.send("noAuth");
      return;
    }


    if (!req.body.data) {
      console.log('/publishphoto => (No data in request)');
      return;
    }

    var data = req.body.data;
    console.log("data.length = " + data.length);

    var buff = new Buffer(data, 'base64');
    var commentText = "by dc";
    if (req.body.txt) {
      commentText = req.body.txt;
    }
    var filename = path.join(process.env.TEMP_DIR, (publishPohotCount++) + '.jpg');
    

    printTime("before writefile");
    fs.writeFile(filename, buff, function (err) {
      if (err) {
        console.log(err);
        res.send(err.message);
        fs.unlink(filename);
        return;
      }
      printTime("writefile ok ");
      var imgUrl = "http://" + req.host + "/upload/" + path.basename(filename);
      console.log(imgUrl);

      var photoPost = {
        message: commentText
        , url: imgUrl
      }
      console.log('/publishphoto => (me/photos graph before Graph Rqst)');
      graph.post('/me/photos', photoPost, req.session, function (err, graphRslt) {

        console.log('/publishphoto => (me/photos graph repsonse)');
        if (err) {
          if (err.code == 2) {
            console.log("unknow error code 2, contniue as there was no error");
            var msg = { postid: '100' };
            res.json(msg);
            console.log("/publishphoto/ [POST] => (SEMI-OK)");
            return;

          }
          console.log('/publishphoto => (Graph Error)');
          console.log(err);
          res.send(err);
          return;
        }
       
        res.send(graphRslt);
       
        console.log('/publishphoto => (OK)');
        //publishSotry(graphRslt.id);
        mongo.addAction(req.session.fb.id);
        //fs.unlink(filename);
       
      });
    });
  });

  var imagecount = 0;
  app.post('/publishphoto2', function (req, res) {

    console.log("publishphoto2 file path =" + req.files.image.path);


    var ulFile = req.files.image.path;
    var imgUrl = "http://" + req.host + "/upload/" + path.basename(ulFile);

    console.dir(req.body);

    var commentText = "by dc";
    if (req.body.txt) {
      commentText = req.body.txt;
    }

    var photoPost = {
      message: commentText,
      url: imgUrl
    };
    console.log(imgUrl);
    graph.post('/me/photos', photoPost, req.session, function (err, graphRslt) {
      console.log('/publishphoto => (me/photos graph repsonse)');
      if (err) {
        console.log('/publishphoto => (Graph Error)');
        console.log(err);
        res.send(err);
        return;
      }

      res.send(graphRslt);

      console.log('/publishphoto => (OK)');
      //publishSotry(graphRslt.id);
      mongo.addAction(req.session.fb.id);
      //fs.unlink(filename);


    });

  });


  function publishSotry(imgId) {
    console.log("publishSotry IN");
    var imgUrl;
    var imgQuery = imgId +"?fields=picture";
    console.log(imgQuery);
    graph.get(imgQuery, function (er, res) {
      if (er) {
        console.log("image query faild");
        return;
      }

      console.log("image query success source = "+ res.source);

      storyPost = {
        image: res.source,
        type: 'drawing',
        title: 'Draw comment Darwing',
      }

      graph.post('/me/objects/draw-comment:drawing', storyPost
        , function (err, graphRslt) {

          if (err) {

            console.log(JSON.stringify(err));
            return;
          }
          console.log("publishSotry OK");
          console.log(JSON.stringify(graphRslt));
        });
    });
  }


  app.get('/isAuth', function (req, res) {

    var isok = graph.isAuth(req.session);
    console.log(req.session);
    res.send(isok);


  });


  app.get('/addreport', function (req, res) {

    var name = req.query.name;
    if (!name) {
      res.send("no name param");
      return;
    }
    mongo.addReport(name);
    res.send("report added");

  });

  app.get('/reports', function (req, res) {

    mongo.getReports(function (docs) {

      res.send(docs);
    });
  });


  function syncDrawingToCDN(res, imgId, cmntId, data) {

    //console.log('==>syncDrawingToCDN = path' + imgId + "/" + cmntId)
    amazonClient.uploadDrawing(imgId + "/" + cmntId, data).then(function (rslt) {
      //console.log("amazonClient.uploadDrawing Promised returned " + rslt);
      res.send(rslt);
    });

  }






  function printTime(msg) {
    return;
    var d = new Date();
    console.log(msg);
    console.log("TIME ==> "+d.getMinutes() + " : " + d.getSeconds() + ": " + d.getMilliseconds());
  }






}