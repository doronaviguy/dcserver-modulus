
///**********************************//
/// <reference path="main.js" />
/// <reference path="paintFabric.js" />
/// <reference path="proxy.js" />
///*********************************//

function log(msg) {
  console.log(msg);
}

var isDcLinkAction = false;
window.onpopstate = function () {
  if (isDcLinkAction) {
    isDcLinkAction = false;
    $("div.stage img.spotlight").waitUntilExists(function () {
      alert("exists");
      DC.showPaint(theaterImg);
    });
    
  }
  Paint.hide();

}



var mediaClassCntnr = ".uiInlineBlock .uiInlineBlockMiddle .fbPhotosPhotoActions";
var mediaClassInnr = ".fbPhotosPhotoActionsTag .tagButton .buttonLink";
var uiActionClassCntnr = "#mainContainer .UIActionLinks.UIActionLinks_bottom";
var uiActionClassInnr = ".uiLinkButton .comment_link";
var NEWS_FEED_IMGS = ".mainWrapper .photoRedesign a";
var NEWS_FEED_PRNT = ".mainWrapper";
var NEWS_FEED_CMNTS = ".uiUfi.UFIContainer";
var TIME_LINE_NEWS_FEED_IMGS = ".uiScaledImageContainer.photoWrap";
var TIME_LINE_PRNT = ".timelineUnitContainer";
var TIME_LINE_CMNTS = ".uiUfi.UFIContainer";
var THEATER_COMMENTS_CNTNR = "#fbPhotoSnowliftFeedback ";

jQuery.noConflict();
var J = jQuery;

/* Draw Comment Main Object*/
var DC = (function ($) {
  var scrolOfst = 0;
  var docHeight = 0;
  var portName = 'dc';
  var drawJsonsMap = {};
  var _paintDOMinit = false;
  var _shouldShowWhenReady = false;

  ///////////////////////////////////////
  var init = function () {

    var pmsgUrl = chrome.extension.getURL('postMessageLinstener.js');
    $("head").first().append("<script src='"+pmsgUrl+"' type='text/javascript'></script>");
    $(document).scrollTop(0);

    $(document).scroll(scrollHndlr);

    //TODO
    setTimeout(function () {
      bindToNewsFeed();
      bindToTimeLine();
    },3000);
    //attachToFeed();
    //addAnchors();
  };
  ///////////////////////////////////////

  
  var getDrawings = function (/*string*/ src, /*$*/ cmntsDOMcntnr, /*$*/imgDOM) {
  
    (function (src, cmntsDOMcntnr, imgDOM) {
      var imgId = src;
      var isRawUrl = !(/\d+$/.test(src));
      if (isRawUrl) 
        imgId = DC.getImgId(src);

      console.log("[getDrawings() ] imgId = " + imgId);

      var rqstClbk = function (/*msgObj {type... ,data ...}*/ rslt) {
       
        console.log("rqstcallback getDrawings");

        var urlsArr = rslt.data;
        if (!urlsArr.length)
          return;
        attach2Cmnts(rslt.data, cmntsDOMcntnr, imgDOM);
      };

      if (imgId) {
        BgProxy.makeRqst({ type: 'getImgsByPostId', postId: imgId }, rqstClbk);
      }

      //console.log("imgId " + imgId);
    })(src, cmntsDOMcntnr, imgDOM);


  };
  ///////////////////////////////////////
  var scrollHndlr = function () {

    if (docHeight != $(document).height()) {
      log("document height changed ");
      docHeight = $(document).height();
      addAnchors();
    }
  };
  ///////////////////////////////////////
  
  var addAnchors = function () {
    log("addAnchors fired");
    (function action() {
      setTimeout(function () {
        var lnks = $(uiActionClassCntnr);
        if (!lnks.length)
          return action();

        $(uiActionClassCntnr).each(function (indx) {
          if ($(this).attr("don"))
            return;

          console.log('==>this.don = ' + $(this).attr("don"))
          $(this).attr("don", 1);

          var el = $("<span><a class='uiLinkLightBlue' >Draw Comment</a> · </span>");


          /* post message becuase of the content script limitations*/
          el.click(function () {
            var imgDom = null;
            imgDom = el.closest(".timelineUnitContainer").find("div[role=article] a[rel=theater]").first();

            if (!imgDom.length)
              imgDom = el.closest(".mainWrapper").find('.photoRedesign a').first();

            if (!imgDom.length) {
              console.log("imgDOM is null [no suitable parent was found [draw comment link]]");
              return false;
            }
            var href = imgDom.attr('href');
            var cmnd = 'document.querySelector("a[href=\'' + href + '\']").click()';

            isDcLinkAction = true; /* popstate event will show Paint*/
            window.postMessage({ cmnd: cmnd }, "*");
          });
          $(this).append(el);
        });
      }, 500)
    })();
  };
  ///////////////////////////////////////
  var _paintCreateDom = function (imgToWrap) {
    if (_paintDOMinit)
      return;
    _paintDOMinit = true;
   
    var img = imgToWrap;
    var imgOffset = img.offset();
    var wrap = $("<div id='dcwrapper' style='position:absolute; top:"
                + imgOffset.top + "px; bottom:0px;"
                + "width:" + img.width()
                + "px; left:" + imgOffset.left + "px; z-index:800;' class='no-seleciton'></div>"
    );

    //console.log('==>dcwarapper right is :' + imgOffset.left + img.find('img.spotlight').width() - $(window).width());

    wrap.append("<canvas id='canvas' class='fbPhotosPhotoTagboxes'></canvas>");

    $('body').append(wrap);

    Paint.init("canvas", imgToWrap);

    $('#canvas').css("border", "1px solid red");
    $('#dcwarpper .canvas-container').animate({ top: "0px", left: "0px", position: 'absolute' });
    //Paint.cnvs.setHeight(img.height());
    //Paint.cnvs.setWidth(img.width());

    wrap.click(function (e) {
      e.preventDefault();
      return false;
    });

    Paint.initToolBox(wrap);

  }
  ////////////////////////////////////////

  var showPaint = function (imgToWrap, isReadOnly) {
    
    if ($("#canvas:visible").length) {
      console.log("canvas is visible!! ");
      return;
    }
    _paintCreateDom(imgToWrap);
    Paint.setCnvsPrnt(imgToWrap);


    $("#snowliftStageActions").hide();

    $('#dcwrapper').show();

    Paint.readOnlyMode(!isReadOnly);
    _setPaintVisible(false);
  };

  ///////////////////////////////////////
  var attach2Cmnts = function (/*[url,url]*/cmntsUrlArr, /*$*/cmntsDOMcntnr, /*$*/imgDOM) {

    console.log('==>attach2Cmnts called');
    if (cmntsDOMcntnr.length == 0)
        return;

    var allPostCmntHref = cmntsDOMcntnr.find('.uiCloseButton.UFICommentCloseButton');
    var cmntsIdsArr = [];

    console.log('allPostCmntHref ' + allPostCmntHref + " allPostCmntHref " + allPostCmntHref.length);

    if (!allPostCmntHref || !allPostCmntHref.length)
      return;

    allPostCmntHref.each(function (it /*href*/) {
      var cmntId = $(this).prop('id').match(/_([^}][0-9]*)/);
      if (cmntId && cmntId.length > 1) {
        cmntId = cmntId[1];
        console.log(cmntId);
        cmntsIdsArr.push(cmntId);
      }
    });
    console.log('==> cmntsIdsArr.length');

    if (!cmntsIdsArr.length)
      return;

    var pos, lastBckSlash;
    for (var i = 0; i < cmntsIdsArr.length; i++) {
      for (var j = 0; j < cmntsUrlArr.length; j++) {


        pos = cmntsUrlArr[j].indexOf(cmntsIdsArr[i]);
        lastBckSlash = cmntsUrlArr[j].indexOf('/');
        if (pos > lastBckSlash) {

          addDrawToCmnt($(allPostCmntHref.get(i)), cmntsUrlArr[j], imgDOM);
        }
      }
    }
    // $("#dcwrapper").on("mouseover", function (e) {
    //  console.log("imgDOM mouseover");
    //  $("#dcwrapper").hide();
    //});

  };
  //////////////////////////////////////////////////////////////////////////////
  var addDrawToCmnt = function (/*$*/domEl, /*url*/drawDataUrl, /*$*/ imgDOM) {

    var $btn = $("<span class='paltBtn' jsonurl='" + drawDataUrl+ "'></span>");

    if (domEl.parent().parent().find('.UFICommentBody .paltBtn').length != 0)
      return;

    domEl.parent().parent().find('.UFICommentBody').append($btn);
    domEl = $btn;


    if (!domEl) {
      console.log('==>domEl is not defined .... !! !! ! ')
    }


    if (!domEl.length) {
      console.log('==> not found (addDrawToCmnt)');
      return false;

    }
    //////////////////////////////////////////////////
    var getDrawJsonCallback =  function (/*{}*/ res, el) {

      console.log('==>getDrawJson [jsonurl ]= ' + el.jsonUrl + ' [res.data] ' + res.data.length);

      el.cache = { it: res.data };
      Paint.setCnvsPrnt(imgDOM);
      Paint.loadJson(res.data);

    };
    //////////////////////////////////////////////////
    var getDrawingsLock = 0;
    domEl.hover(/*mosueIn*/function () {

      var d1 = new Date();
      if (getDrawingsLock && (d1 - getDrawingsLock) < 1000) {
        console.log("######################################## getDrawingsLock is locking");
        return;
      }
      getDrawingsLock = d1;

      console.log("============================================== hover !!!! timeDiff = " + (d1 - getDrawingsLock));

      console.log('hover url ='+ $(this).attr("jsonurl"));
      if (!drawDataUrl) {
        console.log('==>   drawDataUrl is null ');
        return;
      }
      

      showPaint(imgDOM, true);
      var that = $(this).get(0); /* saving the current DOM elemnt non Jquery */
      

      if (that.cache && that.cache.it) {
        getDrawJsonCallback({ data: that.cache.it }, that);
        console.log("load json called form cache , cache length is => " + that.cache.it.objects.length);
      }
      else {
        console.log("BgProxy called");
      
        BgProxy.makeRqst({ type: 'getDrawJson', jsonUrl: drawDataUrl },
          function (res) {
            getDrawJsonCallback(res, that);
        });
      }
    }, /*mouseout*/
    function () {
      Paint.hideVisual();
    });
  };
  ////////////////////////////////////////////////////////////////////////
  var attachToFeed = function () {
    console.log("attach to feeed called length = " + $(uiActionClassCntnr).length);


    $(uiActionClassCntnr).append($("<span>Draw Comment</span>"));

  }
  ///////////////////////////////////////////////////////////////////////
  var getImgId = function (src) {
    if (!src)
      return -1;
    var imgId = src.match(/fbid=([^&]*)/);
    if (imgId && imgId.length > 1)
      imgId = imgId[1];
    imgId = imgId;
    if (imgId)
      return imgId;


    console.log("getimgdID Regex faild! ! ! !");

    var fbidArr = [];
    $.each($("div.stage a[href]"),
    function (i) {

      var posA = $(this).attr('href').indexof('fbid');
      if (posA > 0) {
        var posE = $(this).attr('href').indexof(posA, '&');
        fbidArr.push($(this).attr('href').substr(posA, posE - posA));
      }
    });

    console.log(fbidArr);
    DC.imgId = fbidArr[0];
    return fbidArr[0];
  }
  ///////////////////////////////////////////////////////////////////////

  var _setPaintVisible = function (val) {
    _shouldShowWhenReady = val || true;
  };
  ///////////////////////////////////////////////////////////////////////
  var _getPaintVisible = function () {
    return _shouldShowWhenReady || false;
  };
  ///////////////////////////////////////////////////////////////////////
  function bindToTimeLine() {

    var tlImgs = $(TIME_LINE_NEWS_FEED_IMGS);
    console.log("Bind to Time Line Items found " + tlImgs.length);

    $.each(tlImgs, function (i) {
      
      (function (/*$*/ it) {
       
        if (!it) return;
        it = it.parent();

        var imgId = it.attr('href').match(/fbid=([^&]*)/);
        if (imgId.length < 1)
          return;
        imgId = imgId[1];
        var cmntsDom = it.closest(TIME_LINE_PRNT).find(TIME_LINE_CMNTS);

        if (cmntsDom.length)
          DC.getDrawings(imgId, cmntsDom, it);

      })($(this));
    });
  }
  ///////////////////////////////////////////////////////////////////////
  function bindToNewsFeed() {

    
    var newsFeedImgs = $(NEWS_FEED_IMGS);

    console.log("Bind to News Feed NEWS_Image Items found "+ newsFeedImgs.length);

    $.each(newsFeedImgs, function () {

      //encapsulate the itterator .. there is an Ajax request per Image
      (function (/*$*/it) {

        if (!it) return;

        var imgSrc = it.attr('ajaxify');
        if (!imgSrc)
          return;

        var cmntsDom = it.closest(NEWS_FEED_PRNT).find(NEWS_FEED_CMNTS);
        if (cmntsDom.length)
          DC.getDrawings(imgSrc, cmntsDom, it);

      })($(this));

    });
  };
  ///////////////////////////////////////////////////////////////////////
  //Public
  return {
    init: init,
    getDrawings: getDrawings,
    setPaintVisible: _setPaintVisible,
    getPaintVisible: _getPaintVisible,
    showPaint: showPaint,
    getImgId: getImgId,
    drawJsonsMap: drawJsonsMap
  }



})(jQuery);


(function bindToDOM($) {

  var oldLocation = location.href; /* user is viewing a photo*/
  setInterval(function () {
    if (location.href != oldLocation) {
      if (location.href.indexOf("&theater") > 10)
        bindToTheater();
      else
        return;
      
      oldLocation = location.href
    }
  }, 1000);

  ///////////////////////////////////////////////////////////////////////
  function bindToTheater() {

    var overLayBtn = $(" <div class='" + mediaClassCntnr + "'><a class='" + mediaClassInnr + " mediaDrawComment'>Draw Comment</a> ");

    var theaterImg = $("div.stage img.spotlight");

    if (DC.getPaintVisible()) {
      DC.showPaint(theaterImg);
    }
    if ($('.overlayBarButtons .mediaDrawComment').length != 0)
      return;

    overLayBtn.click(function () {
      DC.showPaint(theaterImg);
    });

    $(".overlayBarButtons").append(overLayBtn);
    setTimeout(function () {
      var imgId = DC.getImgId(document.location.href);
      DC.getDrawings(imgId, $(THEATER_COMMENTS_CNTNR), theaterImg);
    }, 1000);
  };


  ///////////////////////////////////////////////////////////////////////
  DC.init();

})(jQuery);




var BgProxy = (function () {
  var _port = null, sessionCounter = 0, _portCount = 0;

  ////////////////////////////////////////
  var _makeRqst = function (data, clbk) {
    
    _getPort();
    console.dir("BgProxy called with ");
    console.dir(data);

    _validatePort();

    console.log("make request called [times " + (sessionCounter++) +"] ");
    _port.postMessage(data);
    _port.onMessage.addListener(BgProxy.onRqst.bind(clbk));

  };
  ////////////////////////////////////////
  var _onRqst = function (msg) {
    
    _port.onMessage.removeListener(arguments.callee);
    console.dir(msg);
    if (msg.type) {
    
      console.log('==>callback called =>' + msg + " : " + msg.type);
      this(msg);
    }
  };
  ////////////////////////////////////////
  var _getPort = function () {
    _portCount++;
    _port = null;
    _port = chrome.extension.connect({ name: 'dc'+_portCount });
  }
  ////////////////////////////////////////
  var _validatePort = function () {
    if (_port)
      return;
    _port = chrome.extension.connect({ name: 'dc'+_portCount });
    _port.onDisconnect = function () {
      _port = null;
    };
  }

  //Public
  return {
    makeRqst: _makeRqst,
    onRqst: _onRqst
  };
})();



//baseUrl: 'http://dcapi.jit.su'



(function ($) {

  /**
  * @function
  * @property {object} jQuery plugin which runs handler function once specified element is inserted into the DOM
  * @param {function} handler A function to execute at the time when the element is inserted
  * @param {bool} shouldRunHandlerOnce Optional: if true, handler is unbound after its first invocation
  * @example $(selector).waitUntilExists(function);
  */

  $.fn.waitUntilExists = function (handler, shouldRunHandlerOnce, isChild) {
    var found = 'found';
    var $this = $(this.selector);
    var $elements = $this.not(function () { return $(this).data(found); }).each(handler).data(found, true);

    if (!isChild) {
      (window.waitUntilExists_Intervals = window.waitUntilExists_Intervals || {})[this.selector] =
        window.setInterval(function () { $this.waitUntilExists(handler, shouldRunHandlerOnce, true); }, 500)
      ;
    }
    else if (shouldRunHandlerOnce && $elements.length) {
      window.clearInterval(window.waitUntilExists_Intervals[this.selector]);
    }

    return $this;
  }

}(jQuery));


/*"content_security_policy": "default-src 'none'; style-src 'self'; script-src 'self'; connect-src http://dcserver.doron.c9.io/getCS; " ,*/