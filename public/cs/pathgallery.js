
var pathGallery = (function (prms) {
  var $ = jQuery;
  var queryParam = '';
  var init = function ($prntDom) {
    intDomCntnr($prntDom);
  };

  var intDomCntnr = function ($prntDom) {
    var $cntnr = $("<div id='pathgallery'><div class='searchArea'><input type='text' id='pathgallerySearch' />"
                  + " <input id='pathgallerySearchBtn' type='button' value ='find'> </div>"
                  + "<div class='resultsArea'></div></div>");
    $prntDom.append($cntnr);

    $('#pathgallerySearchBtn').click(onSearch);

  }

  var onSearch = function () {
    var txt = $('#pathgallerySearch').val();
    $.get(prms.apiUrl + txt + '&page=1&results=40&format=json', null, searchClbk)
  };

  var searchClbk = function (rslt) {

    var data = eval('(' + rslt + ')');
    if (data && data.msg && data.msg == 'success') {

      data = data.payload;
      var $img, imgData;
      var rsltcntnr = $('#pathgallery .resultsArea');
      for (var i = 0; i < data.length; i++) {

        if (data[i] && data[i].svg) {
          imgData = data[i].svg;
          $img = $("<img src='" + imgData.png_thumb + "' svg='" + imgData.url + "' title='" + data[i].title + "'");
          rsltcntnr.append($img);
        }
      }

    }

  }

  return {
    init: init
  };
})({
  apiUrl: "http://openclipart.org/search/json/?query=",
  domCntnr: ""

});