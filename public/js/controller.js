

function UsersCtrl($scope, $http) {

  $scope.users = [];
  $scope.totalActions = 0;;

  $http.get('/getdbdata').success(function (res) {
    console.dir(res);
    $scope.users = res;
    getActionsCount();
  }).error(function (res) {
    console.log('error =>'+res);
  });;

  var getActionsCount = function () {
    $scope.totalActions;
    for (var i = 0; i < $scope.users.length; i++) {
      if ($scope.users[i] && $scope.users[i].actions)
        $scope.totalActions += $scope.users[i].actions;
    }
    return $scope.totalActions;
  };
  

  $scope.getuserImg = function (id) {
    return "http://graph.facebook.com/" + id + "/picture?type=square";
  }
  $scope.navToProfile = function (id) {

    var url = "https://www.facebook.com/profile.php?id=" + id;
    var a = document.createElement("a");
    a.setAttribute("target", "_blank");
    a.href = url;
    a.click();
    //document.location.href = url;
  }

}



function ReportsCtrl($scope, $http) {

  $scope.reports = [];
  $scope.reportsAll = [];

  $http.get('http://api.drawcomment.com/reports').success(function (res) {
    console.dir(res);
    $scope.reports = res;
    $scope.reportsAll = res.slice(0); // clone array 
  }).error(function (res) {
    console.log('error =>' + res);
  });;

  $scope.getCount = function(rprt) {
    return rprt.dates.length || 0;
  };

  $scope.groupByToday = function () {
    
    for (var i = 0; i < $scope.reportsAll.length; i++) {
      getReprotsDim($scope.reportsAll[i], 1, $scope.reports[i]);
    }
  }

  $scope.groupBy2Days = function () {
    for (var i = 0; i < $scope.reportsAll.length; i++) {
      getReprotsDim($scope.reportsAll[i], 2, $scope.reports[i]);
    }
  }

  $scope.groupByWeek = function () {
    for (var i = 0; i < $scope.reportsAll.length; i++) {
      getReprotsDim($scope.reportsAll[i], 7, $scope.reports[i]);
    }
  }

  function getReprotsDim(rprt, by, trgt) {
    by = by || 1;
    var res = [];
    var yestrday = new Date() - 86400000 * by;
    for (var i = 0; i < rprt.dates.length; i++) {
      if (new Date(rprt.dates[i]) > yestrday)
        res.push(rprt.dates[i]);
    }
    console.log(res.length);
    trgt.dates = res;

  }

}