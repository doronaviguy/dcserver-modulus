


var mongoose = require('mongoose');
var mongo = process.env.MONGO_URL;

var generate_mongo_url = function (obj) {
  if (mongo)
    return mongo;
  else
    return 'mongodb://localhost/test';
}
var mongourl = generate_mongo_url(mongo);



mongoose.connect(mongourl);


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongo db connection error:'));
db.once('open', function callback() {
  console.log("mongo db is open");
});


var userSchema = mongoose.Schema({
  name: {
    type: String
    , required: true
    , unique: true
  },
  id: {
    type: Number
    , required: true
    , unique: true
  },
  actions: Number,
  logins: Number
});

userSchema.methods.print = function () {
  console.log("print() =>" + print());
};

var User = mongoose.model('User', userSchema);

var reportSchema = mongoose.Schema({
  name: {
    type: String
    , required: true
    , unique: true
  },
  dates:Array
});
var Report = mongoose.model("Report", reportSchema);



exports.addAction = function (id) {

  User.findOne({ id: id }, function (err, doc) {
    if (err) {
      console.log('addAction() faild '+ err);
      return;
    }
    if (!doc) {
      console.log('addAction() faild doc not found');
      return;
    }
    doc.actions = doc.actions + 1;
    doc.save(function (err, doc) {
      console.log("doc updated [addAction]");
    });
  });

}

exports.addUser = function (name, id) {
  if (!name || !id) {
    console.log('name is not defined');
    return;
  }
  User.findOne({ name: name }, function (err, doc) {

    if (doc == null) {
      var user = new User({ name: name, id: id, actions: 0, logins: 0 });
      user.save(function (err, user) {
        if (err) throw err;
        console.log("user saved succesfully");
      });
    } else {
      doc.logins = doc.logins + 1;
      doc.save(function (err, doc) {
        if (err) {
          console.log(err);
          return;
        }
        console.log("doc updated [addUser]");
      });
    }


  });


}

exports.getUsers = function (clbk) {

  User.find(function (err, rslt) {
    if (err) {
      console.log("mongo " + err);
      clbk(null);
      return;
    }
    clbk(rslt);
  });
}


exports.addReport = function (reportName) {

  Report.findOne({ name: reportName }, function (err, doc) {
    if (doc == null) {
      var rprt = new Report({ name: reportName, dates: [new Date()] });
      rprt.save(function (err, user) {
        if (err) {
          console.log(err);
          return;
        }

        //console.log("report saved succesfully");
      });
    } else {
      doc.dates.push(new Date());
      doc.save(function (err, doc) {
        if (err) {
          console.log(err);
          return;
        }
        //console.log("doc updated [addReport]");
      });
    }
  });


}

exports.getReports = function (clbk) {

  Report.find(function (err, rslt) {
    if (err) {
      console.log("mongo " + err);
      clbk(null);
      return;
    }
    clbk(rslt);
  });


}


exports.clear = function () {
  return;
  User.find(function (err, docs) {
    if (err) {
      console.log("mongo " + err);
    }
    docs.forEach(function (doc) {
      doc.remove();
    });
  });


}













